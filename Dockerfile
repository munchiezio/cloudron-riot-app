FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code /app/data

WORKDIR /app/code

EXPOSE 8000

ENV RIOTVERSION=v1.0.8

# riot
RUN curl -L https://github.com/vector-im/riot-web/releases/download/${RIOTVERSION}/riot-${RIOTVERSION}.tar.gz | tar -xz --strip-components 1 -f -
RUN ln -sf /app/data/riot_config.json /app/code/config.json

# nginx
RUN rm /etc/nginx/sites-enabled/* && \
    rm -rf /var/lib/nginx && ln -sf /run/nginx /var/lib/nginx && \
    rm -rf /var/log/nginx && ln -sf /run/nginx_log /var/log/nginx
ADD nginx_riot.conf /etc/nginx/sites-enabled/

RUN chown -R www-data.www-data /app/code /app/data

ADD start.sh /app/

CMD [ "/app/start.sh" ]
