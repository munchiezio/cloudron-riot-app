#!/bin/bash

set -eux

if [[ ! -e /app/data/riot_config.json ]]; then
    echo "=> Detected first run"
    cp /app/code/config.sample.json /app/data/riot_config.json
    sed -i "s#https://matrix.org#https://matrix.irgendwo.co#" /app/data/riot_config.json
fi

mkdir -p /run/nginx /run/nginx_log

chown -R www-data.www-data /app/data /run/

exec /usr/sbin/nginx -g 'daemon off;'
